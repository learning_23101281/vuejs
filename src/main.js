import "./assets/main.css";
import "primeicons/primeicons.css";
import router from "@/router";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

import { createApp } from "vue";
import App from "./App.vue";

// Create the Vue app instance
const app = createApp(App);

// Use Modules
app.use(router); // Use the router
app.use(Toast); // Use the Toast module

// Mount the app to the DOM (in this case, the #app div in public/index.html)
app.mount("#app");
